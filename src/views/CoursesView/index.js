export const CoursesView = ({ courses, profile }) => (
    <div className = 'container-fluid'>
        <div className = 'row'>
            <div className = 'col-xl-9 col-lg-8'>
                { courses }
            </div>
            <div className = 'col-xl-3 col-lg-4'>
                <div className = 'right_side'>
                    { profile }
                </div>
            </div>
        </div>
    </div>
);

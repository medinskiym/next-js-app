// Components
import { Footer } from '../../components/Footer';
import { Menu } from '../../components/Menu';

export const AppView = ({ children }) => (
    <>
        <Menu />
        <div className = 'wrapper'>
            <div className = 'sa4d25'>
                { children }
                <Footer />
            </div>
        </div>
    </>
);

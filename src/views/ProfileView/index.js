import { ProfileTabs } from '../../components/ProfileTabs';

export const ProfileView = ({ children, fullName, jobTitle }) => (
    <>
        <div className = '_216b01'>
            <div className = 'container-fluid'>
                <div className = 'row justify-content-md-center'>
                    <div className = 'col-md-10'>
                        <div className = 'section3125 rpt145'>
                            <div className = 'row'>
                                <div className = 'col-lg-7'>
                                    <div className = 'dp_dt150'>
                                        <div className = 'img148'>
                                            <img src = '/images/hd_dp.jpg' alt = '' />
                                        </div>
                                        <div className = 'prfledt1'>
                                            <h2>{ fullName }</h2>
                                            <span>{ jobTitle }</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className = '_215b15'>
            <div className = 'container-fluid'>
                <div className = 'row'>
                    <div className = 'col-lg-12'>
                        <ProfileTabs />
                    </div>
                </div>
            </div>
        </div>
        <div className = '_215b17'>
            <div className = 'container-fluid'>
                <div className = 'row'>
                    <div className = 'col-lg-12'>
                        <div className = 'course_tab_content'>
                            <div className = 'tab-content' id = 'nav-tabContent'>
                                <div
                                    className = 'tab-pane fade show active'
                                >
                                    { children }
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
);

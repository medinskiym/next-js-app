export const routes = {
    homepage: {
        path: '/',
        page: '/index',
    },
    teacher: {
        about: {
            path: '/teacher/about',
            page: '/teacher/about',
        },
        courses: {
            path: '/teacher/courses',
            page: '/teacher/courses',
        },
    },
};

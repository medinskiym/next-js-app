import { Course } from '../Course';

export const Courses = () => (
    <div className = 'section3125'>
        <div className = 'la5lo1'>
            <div className = 'featured_courses'>
                { [...new Array(9).keys()].map((index) => (<Course key = { index } />)) }
            </div>
        </div>
    </div>
);

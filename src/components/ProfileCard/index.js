export const ProfileCard = () => (
    <div className = 'fcrse_2 mb-30'>
        <div className = 'tutor_img'>
            <a href = '#' aria-label = 'profile'><img src = '/images/hd_dp.jpg' alt = '' /></a>
        </div>
        <div className = 'tutor_content_dt'>
            <div className = 'tutor150'>
                <a href = '#' className = 'tutor_name'>Joginder Singh</a>
                <div className = 'mef78' title = 'Verify'>
                    <i className = 'uil uil-check-circle' />
                </div>
            </div>
            <div className = 'tutor_cate'>
                Web Developer, Designer, and Teacher
            </div>

            <div className = 'tut1250'>
                <span className = 'vdt15'>615K Students</span>
                <span className = 'vdt15'>12 Courses</span>
            </div>
            <a href = '#' className = 'prfle12link'>Go To Profile</a>
        </div>
    </div>
);

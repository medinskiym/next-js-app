import NavLink from 'next/link';
import cn from 'classnames';
import { useRouter } from 'next/router';
import { routes } from '../../routes';

const PROFILE_MENU = [{
    label: 'About',
    path: routes.teacher.about.path,
}, {
    label: 'My courses',
    path: routes.teacher.courses.path,
}];

const ProfileTabs = () => {
    const { pathname } = useRouter();

    const menuJSX = PROFILE_MENU.map((menuItem) => (
        <NavLink href = { menuItem.path } key = { menuItem.path }>
            <a
                className = { cn('nav-item nav-link', {
                    active: pathname === menuItem.path,
                }) }
                aria-selected = { pathname === menuItem.path }
            >
                { menuItem.label }
            </a>
        </NavLink>
    ));

    return (
        <div className = 'course_tabs'>
            <nav>
                <div
                    className = 'nav nav-tabs tab_crse'
                    role = 'tablist'
                >

                    { menuJSX }

                </div>
            </nav>
        </div>
    );
};

export { ProfileTabs };

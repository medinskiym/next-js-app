// Views
import { AppView } from '../views/AppView';
import { CoursesView } from '../views/CoursesView';

// Components
import { ProfileCard } from '../components/ProfileCard';
import { Courses } from '../components/Courses';

const Home = () => (
    <AppView>
        <CoursesView
            courses = { <Courses /> }
            profile = { <ProfileCard /> }
        />
    </AppView>
);

export default Home;

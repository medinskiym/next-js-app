// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export default function handler(req, res) {
    res.status(200).json({ name: 'John Doe' });
}

// Components
import { CustomError } from '../components/CustomError';

const Error404 = () => (
    <CustomError statusCode = { 404 } title = 'The page you were looking for could not be found.' />
);

export default Error404;

// Styles
import '../styles/style.css';
import '../styles/bootstrap.min.css';

const MyApp = ({ Component, pageProps }) => <Component { ...pageProps } />;

export default MyApp;

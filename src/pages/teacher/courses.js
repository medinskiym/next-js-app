import { AppView } from '../../views/AppView';
import { ProfileView } from '../../views/ProfileView';
import { Course } from '../../components/Course';
import { USER } from './about';

const COURSE_DATA = [...new Array(2).keys()];

const Courses = () => (
    <AppView>
        <ProfileView fullName = { USER.fullName } jobTitle = { USER.jobTitle }>
            <div className = 'crse_content'>
                <h3>
                    My courses (
                    { COURSE_DATA.length }
                    )
                </h3>
                <div className = '_14d25'>
                    <div className = 'row'>
                        <div className = 'col-12'>
                            <div className = 'section3125'>
                                <div className = 'la5lo1'>
                                    <div className = 'featured_courses'>
                                        { COURSE_DATA.map((index) => (<Course key = { index } />)) }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProfileView>
    </AppView>
);

export default Courses;

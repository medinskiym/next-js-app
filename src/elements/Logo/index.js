export const Logo = () => (
    <div className = 'main_logo' id = 'logo'>
        <a href = '#' aria-label = 'logo'><img src = '/images/logo.svg' alt = '' /></a>
        <a href = '#'>
            <img
                className = 'logo-inverse'
                src = 'images/ct_logo.svg'
                alt = ''
            />
        </a>
    </div>
);
